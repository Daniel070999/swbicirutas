<?php

namespace App\Http\Controllers;

use App\Coordinador;
use App\Bicicleta;
use App\Ruta;
use App\Http\foreign;
use App\Http\integer;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class CoordinadorControlador extends BaseController{

    public function verTodos(Request $request){
    	$coordinador = Coordinador::all();
			if(!$coordinador->isEmpty()){
	    		$status = true;
	    		$info = "data is listed successfully";
	    	}else{
	    		$status = false;
	    		$info = "data is not listed successfully";
	    	}
	    	return ResponseBuilder::result($status, $info, $coordinador, 200);
    	    
    	//return response()->json($coordinador, 200);
    }
    public function getCoordinador(Request $request, $cedula){
	    	$coordinador = Coordinador::where('cedula',$cedula)->get();
	    	if(!$coordinador->isEmpty()){
	    		$status = true;
	    		$info = "data is listed successfully";
	    	}else{
	    		$status = false;
	    		$info = "data is not listed successfully";
	    	}
	    	return ResponseBuilder::result($status, $info, $coordinador, 200);
	}
    public function nuevoCoordinador(Request $request){
    	$coordinador = new Coordinador();
	    $coordinador->cedula = $request->cedula;
	    $coordinador->nombres = $request->nombres;
	    $coordinador->apellidos = $request->apellidos;
	    $coordinador->direccion = $request->direccion;
	    $coordinador->celular = $request->celular;
	    $coordinador->genero = $request->genero;
	    $coordinador->save();
	    $ruta = new Ruta();
	    $ruta->distancia = $request->distancia;
	    $ruta->Niveldificultad = $request->Niveldificultad;
	    $ruta->ProbabilidadClima = $request->ProbabilidadClima;
	    $ruta->lugar = $request->lugar;
	    $ruta->fecha = $request->fecha;
	    $ruta->hora_inicio = $request->hora_inicio;
	    $ruta->hora_final = $request->hora_final;
	    $ruta->coordinador_id = $coordinador->coordinador_id;
	    $ruta->save();

	    $status = true;
	    $info = "data is listed successfully";
	    return ResponseBuilder::result($status, $info, $coordinador,$ruta, 200);
	}
	public function nuevaRuta(Request $request){
    	$coordinador = Coordinador::where('cedula', $request->cedula)->first();
    	$ruta = new Ruta();
	    $ruta->distancia = $request->distancia;
	    $ruta->Niveldificultad = $request->Niveldificultad;
	    $ruta->ProbabilidadClima = $request->ProbabilidadClima;
	    $ruta->lugar = $request->lugar;
	    $ruta->fecha = $request->fecha;
	    $ruta->hora_inicio = $request->hora_inicio;
	    $ruta->hora_final = $request->hora_final;
	    $ruta->coordinador_id = $coordinador->coordinador_id;
	    $ruta->save();

	    $status = true;
	    $info = "data is listed successfully";
	    return ResponseBuilder::result($status,$info,$ruta, 200);
	}
	public function modificarCoordinador(Request $request){
		$coordinador = Coordinador::where('cedula',$request->cedula)->first();
	    $coordinador->cedula = $request->cedula;
	    $coordinador->nombres = $request->nombres;
	    $coordinador->apellidos = $request->apellidos;
	    $coordinador->direccion = $request->direccion;
	    $coordinador->celular = $request->celular;
	    $coordinador->genero = $request->genero;
	    $coordinador->save();

	    $status = true;
	    $info = "data is listed successfully";
	    return ResponseBuilder::result($status,$info,$coordinador, 200);
	}
	public function eliminarCoordinador(Request $request){

		$coordinador = Coordinador::where('cedula',$request->cedula)->first();
		$ruta = Ruta::where('coordinador_id',$coordinador->coordinador_id)->delete();
		$coordinador->delete();
	    $status = true;
	    $info = "coordinador eliminado";
	    return ResponseBuilder::result($status,$info,$coordinador, 200);
	}
	public function eliminarRuta(Request $request){
		Ruta::where('ruta_id',$request->ruta)->delete();	
		$status = true;
	    $info = "ruta eliminada";
	    return ResponseBuilder::result($status,$info, 200);
	}
	public function misRutas(Request $request,$cedula){


		$coordinador = Coordinador::where('cedula',$cedula)->first();
		$ruta = Ruta::where('coordinador_id',$coordinador->coordinador_id)->get();	
		$status = true;
	    $info = "lista de rutas";
	    return ResponseBuilder::result($status,$info,$ruta, 200);
	}
	public function verTodasRutas(Request $request){
    	$ruta = Ruta::all();

   		$status = true;
   		$info = "data is listed successfully";

    	return ResponseBuilder::result($status, $info, $ruta, 200);
    }
    //sera usado unicamente para android, para la accion de un clic en el recyclerView
    public function datosRuta(Request $request,$id){

    	$coordinador = Coordinador::where('coordinador_id',$id)->first();

   		$status = true;
   		$info = "data is listed successfully";

    	return ResponseBuilder::result($status, $info, $coordinador, 200);
    }
    public function verParticipantes(Request $request){
    	$ruta = $request->ruta;
    	$bicicleta = Bicicleta::where('rut_id',$ruta)->get();
    	$bicicleta->

   		$status = true;
   		$info = "data is listed successfully";

    	return ResponseBuilder::result($status, $info, $bicicleta, 200);
    }
}
