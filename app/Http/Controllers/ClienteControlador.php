<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Bicicleta;
use App\Coordinador;
use App\Ruta;
use App\Http\foreign;
use App\Http\integer;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class ClienteControlador extends BaseController{

    public function verTodos(Request $request){
    	$clientes = Cliente::all();
    	if(!$clientes->isEmpty()){
	    		$status = true;
	    		$info = "data is listed successfully";
	    	}else{
	    		$status = false;
	    		$info = "data is not listed successfully";
	    	}
	    	return ResponseBuilder::result($status, $info, $clientes, 200);
    	
//    	return response()->json($clientes, 200);
    }
    public function buscarCliente(Request $request,$cedula){
    	$cliente = Cliente::where('cedula',$cedula)->first();

	    $status = true;
	    $info = "data is listed successfully";
    	return ResponseBuilder::result($status, $info, $cliente, 200);
    	
    }
    public function nuevoCliente(Request $request){
    	$cliente = new Cliente();
	    $cliente->cedula = $request->cedula;
	    $cliente->nombres = $request->nombres;
	    $cliente->apellidos = $request->apellidos;
	    $cliente->direccion = $request->direccion;
	    $cliente->celular = $request->celular;
	    $cliente->genero = $request->genero;
	    $cliente->edad = $request->edad;
	    $cliente->estatura = $request->estatura;
	    $cliente->save();
	    $bicicleta = new Bicicleta();
		$r_id = 0;
	    $bicicleta->chasis = $request->chasis;
	    $bicicleta->nro_aro = $request->nro_aro;
	    $bicicleta->marca = $request->marca;
	    $bicicleta->tipo = $request->tipo;
	    $bicicleta->rut_id = $r_id;
	    $bicicleta->cliente_id = $cliente->cliente_id;
	    $bicicleta->save();

	    $status = true;
	    $info = "data is listed successfully";
    	return ResponseBuilder::result($status, $info, $cliente, 200);
	}
	public function nuevaBicicleta(Request $request){
    	$cliente = Cliente::where('cedula', $request->cedula)->first();
    	$bicicleta = new Bicicleta();
		$r_id = 0;
	    $bicicleta->chasis = $request->chasis;
	    $bicicleta->nro_aro = $request->nro_aro;
	    $bicicleta->marca = $request->marca;
	    $bicicleta->tipo = $request->tipo;
	    $bicicleta->rut_id = $r_id;
	    $bicicleta->cliente_id = $cliente->cliente_id;
	    $bicicleta->save();

	    $status = true;
	    $info = "data is listed successfully";
    	return ResponseBuilder::result($status, $info, $bicicleta, 200);
	}
	public function modificarCliente(Request $request){
		$cliente = Cliente::where('cedula',$request->cedula)->first();
	    $cliente->nombres = $request->nombres;
	    $cliente->apellidos = $request->apellidos;
	    $cliente->direccion = $request->direccion;
	    $cliente->celular = $request->celular;
	    $cliente->edad = $request->edad;
	    $cliente->estatura = $request->estatura;
	    $cliente->save();

	    $status = true;
	    $info = "data is listed successfully";
    	return ResponseBuilder::result($status, $info, $cliente, 200);
	}
	public function eliminarCliente(Request $request){
		$cliente = Cliente::where('cedula',$request->cedula)->first();
		$bicicleta = Bicicleta::where('cliente_id',$cliente->cliente_id)->delete();
		$cliente->delete();
	    $status = true;
	    $info = "cliente eliminado";
	    return ResponseBuilder::result($status,$info,$cliente, 200);
	}
	public function eliminarBicicleta(Request $request){
		$bicicleta = Bicicleta::where('chasis',$request->chasis)->delete();	
   
        $status = true;
	    $info = "bicicleta eliminada";
	    return ResponseBuilder::result($status,$info,$bicicleta, 200);
	}
	public function elegirRuta(Request $request){
		$ruta = Ruta::where('ruta_id',$request->ruta)->first();
		$bicicleta = Bicicleta::where('chasis', $request->chasis)->first();
		if ($bicicleta->rut_id == 0){
			$bicicleta->rut_id = $ruta->ruta_id;
			$bicicleta->save();
			
			$status = true;
	    	$info = "datos procesados correctamente";
		}else if ($bicicleta->rut_id != 0) {
			$status = false;
		    $info = "Usted ya cuenta con una ruta";
		}
    		return ResponseBuilder::result($status, $info, $bicicleta, 200);
	}
	public function cancelarRuta(Request $request){
		$bicicleta = Bicicleta::where('chasis', $request->chasis)->first();
		$rutaID = $bicicleta->rut_id;
		$ruta = Ruta::where('ruta_id',$request->rutaID)->first();
		if ($bicicleta->rut_id != 0){
			$bicicleta->rut_id = "0";
			$bicicleta->save();
		}
			$status = true;
		    $info = "ruta cancelada";
    		return ResponseBuilder::result($status, $info, $bicicleta, 200);
	}
	public function misBicicletas(Request $request,$cedula){
    	$cliente = Cliente::where('cedula',$cedula)->first();
    	$clienteID = $cliente->cliente_id;
    	$bicicleta = Bicicleta::where('cliente_id',$clienteID)->get();

    	if($bicicleta->isEmpty()){
			$status = false;
	    	$info = "data is listed successfully";
    	
    	}else{
			$status = true;
	    	$info = "data is listed successfully";
    	
    	}
    		return ResponseBuilder::result($status, $info, $bicicleta, 200);
    }
    public function miRutaActual(Request $request){
    	$bicicleta = Bicicleta::where('chasis',$request->chasis)->first();
    	$bicicletaID = $bicicleta->rut_id;
    	$ruta = Ruta::where('ruta_id',$bicicletaID)->get();
    	$rutaD = Ruta::where('ruta_id',$bicicletaID)->first();
    	$coordinadorID = $rutaD->coordinador_id;
    	$coordinador = Coordinador::where('coordinador_id',$coordinadorID)->first();
			if ($ruta->isEmpty()) {
				$status = false;
		    	$info = "data is listed successfully";
			}else{
				$status = true;
		    	$info = "data is listed successfully";
			}
    	return ResponseBuilder::result($status, $info, $ruta,$coordinador, 200);
    }
}
