<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bicicleta extends Model {

    protected $table = 'modelo_bicicleta';
    protected $primaryKey = 'bicicleta_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'chasis', 'nro_aro', 'marca', 'tipo', 'rut_id',
    ];
    //realizar borrados logicos
    


    public $timestamps = false;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    /*
    protected $hidden = [
        'password',
    ];
    */

}
