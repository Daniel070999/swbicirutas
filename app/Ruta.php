<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ruta extends Model {

    protected $table = 'modelo_ruta';
    protected $primaryKey = 'ruta_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'distancia', 'Niveldificultad', 'ProbabilidadClima', 'fecha', 'hora_inicio', 'hora_final','lugar',
    ];

    public $timestamps = false;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    /*
    protected $hidden = [
        'password',
    ];
    */

}
