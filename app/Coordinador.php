<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordinador extends Model {

    protected $table = 'modelo_coordinador';
    protected $primaryKey = 'coordinador_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cedula', 'nombres', 'apellidos', 'direccion',  'celular', 'genero',
    ];
   

    public $timestamps = false;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    /*
    protected $hidden = [
        'password',
    ];
    */

}
