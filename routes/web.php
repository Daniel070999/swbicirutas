<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix'=>'cliente'], function($router){
	$router->get('/verTodos','ClienteControlador@verTodos');
	$router->get('/misBicicletas/{cedula}','ClienteControlador@misBicicletas');
	$router->get('/buscarCliente/{cedula}','ClienteControlador@buscarCliente');
	$router->post('/miRutaActual','ClienteControlador@miRutaActual');
	$router->post('/nuevoCliente','ClienteControlador@nuevoCliente');
	$router->post('/nuevaBicicleta','ClienteControlador@nuevaBicicleta');
	$router->post('/modificarCliente','ClienteControlador@modificarCliente');
	$router->post('/eliminarCliente','ClienteControlador@eliminarCliente');
	$router->post('/eliminarBicicleta','ClienteControlador@eliminarBicicleta');
	$router->post('/elegirRuta','ClienteControlador@elegirRuta');
	$router->post('/cancelarRuta','ClienteControlador@cancelarRuta');
});

$router->group(['prefix'=>'coordinador'], function($router){
	$router->get('/datosRuta/{id}','CoordinadorControlador@datosRuta');
	$router->get('/verTodasRutas','CoordinadorControlador@verTodasRutas');
	$router->get('/verTodos','CoordinadorControlador@verTodos');
	$router->get('/misRutas/{cedula}','CoordinadorControlador@misRutas');
	$router->get('/getCoordinador/{cedula}','CoordinadorControlador@getCoordinador');
	$router->post('/nuevoCoordinador','CoordinadorControlador@nuevoCoordinador');
	$router->post('/nuevaRuta','CoordinadorControlador@nuevaRuta');
	$router->post('/modificarCoordinador','CoordinadorControlador@modificarCoordinador');
	$router->post('/eliminarCoordinador','CoordinadorControlador@eliminarCoordinador');
	$router->post('/eliminarRuta','CoordinadorControlador@eliminarRuta');
	$router->post('/verParticipantes','CoordinadorControlador@verParticipantes');
});

$router->group(['prefix'=>'login'], function($router){
	$router->post('/ingresar','UserController@login');
});